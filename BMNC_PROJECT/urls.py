"""BMNC_PROJECT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
import loginlogout.urls as loginlogout
import lihatprofile.urls as profile
import lihatpolling.urls as lihat_polling
import tambah_berita.urls as berita
import buat_polling.urls as polling
import homepage.urls as home
import register.urls as register

urlpatterns = [
    url(r'^profile/', include(('lihatprofile.urls','profile'), namespace='profile')),
    url(r'^lihat-polling/', include(('lihatpolling.urls','lihat_polling'), namespace='lihat-polling')),
    url(r'^berita/', include(('tambah_berita.urls','berita'), namespace='berita')),
    url(r'^polling/', include(('buat_polling.urls','polling'), namespace='polling')),
	url(r'^homepage/', include(('homepage.urls','home'), namespace='home')),
    url(r'^register/', include(('register.urls','register'), namespace='register')),
    url(r'^$', include(('homepage.urls','home'), namespace='home'))
    ]
