from django import forms
from .models import *

class FormBerita(forms.Form):
    judul = forms.CharField(label='Judul', max_length=100, required=True)
    url_berita = forms.URLField(label='URL Berita', max_length=100, required=True)
    topik = forms.CharField(label='Topik',max_length=100, required=True)
    jumlah_kata = forms.IntegerField(label='Jumlah Kata',required=True)
    tag = forms.CharField(label='Tag', max_length=100, required=True)