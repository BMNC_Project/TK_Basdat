from django.shortcuts import render
from django.shortcuts import render_to_response
from django.db import connection

# Create your views here.

response = {}

def show_home(request):
    return render_to_response('bmnc-homescreen.html')

# Create your views here.
def index(request):
	cursor = connection.cursor()
	cursor.execute('select * from berita')
	berita = cursor.fetchall()    
	print (berita)
	response['judul_berita'] = berita[0][1]
	response['judul_berita_1'] = berita[1][1]
	response['judul_berita_2'] = berita[2][1]

	cursor.execute('select * from riwayat')
	riwayat = cursor.fetchall()    
	print (riwayat)
	response['isi_berita'] = riwayat[0][3]
	response['isi_berita_1'] = riwayat[1][3]
	response['isi_berita_2'] = riwayat[2][3]

	return render_to_response('bmnc-register.html')