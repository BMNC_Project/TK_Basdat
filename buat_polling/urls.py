from django.conf.urls import url
from .views import *

#url for app
urlpatterns = [
    url(r'^buat_polling_berita', buat_polling_berita, name='buat_polling_berita'),
    url(r'^buat_polling_biasa', buat_polling_biasa, name='buat_polling_biasa'),
    url(r'^submit_polling_berita', submit_polling_berita, name='submit_polling_berita'),
    url(r'^submit_polling_biasa', submit_polling_biasa, name='submit_polling_biasa'),
]