from django.contrib import messages
from django.db import connection
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from buat_polling.forms import FormPollingBerita, FormPollingBiasa

response = {}
# Create your views here.
def buat_polling_berita(request):
    html = 'buat_polling_berita.html'
    return render(request, html, response)

def buat_polling_biasa(request):
    html = 'buat_polling_biasa.html'
    return render(request, html, response)

@csrf_exempt
def submit_polling_berita(request):
    form = FormPollingBerita(data=request.POST)

    if (request.method == 'POST' and form.is_valid()):
        cursor = connection.cursor()
        query = ('select count(*) from Polling')
        cursor.execute(query)
        count = cursor.fetchone()[0] #(2,)

        id_polling = 0
        if count > 0:
            id_polling = count + 1
        else:
            id_polling = 1

        url_berita= request.POST.get('url_berita', False)#request.POST.get['username']
        waktu_mulai = request.POST.get('waktu_mulai', False)
        waktu_selesai = request.POST.get('waktu_selesai', False)
        pertanyaan = request.POST.get('pertanyaan', False)
        pilihan = request.POST.get('pilihan', False)

        #insert new polling
        query = ("insert into Polling (id, polling_start, polling_end, total_responden) values (" + str(id_polling) + ",'"
            + str(waktu_mulai) + "','" + str(waktu_selesai) + "'," + str(0) + ");" )
        cursor.execute(query)

        #insert new polling berita
        query = "insert into Polling_berita (id_polling, url_berita) values (" + str(id_polling) + ",'" + str(url_berita) +"');"
        cursor.execute(query)

        messages.success(request, "News Polling Submitted!")
        return HttpResponseRedirect(reverse('apps-polling:polling-page'))
    else :
    	return HttpResponse("null")


@csrf_exempt
def submit_polling_biasa(request):
    form = FormPollingBiasa(data=request.POST)

    if (request.method == 'POST' and form.is_valid()):
        cursor = connection.cursor()
        cursor.execute('select count(*) from Polling')
        count = cursor.fetchone()[0] #(2,)

        id_polling = 0
        if count > 0:
            id_polling = count + 1
        else:
            id_polling = 1

        deskripsi = request.POST.get('deskripsi', False)#request.POST.get['username']
        url = request.POST.get('url', False)
        waktu_mulai = request.POST.get('waktu_mulai', False)
        waktu_selesai = request.POST.get('waktu_selesai', False)
        pertanyaan = request.POST.get('pertanyaan', False)
        pilihan = request.POST.get('pilihan', False)

        # insert new polling
        query = ("insert into Polling (id, waktu_mulai, waktu_selesai, total_responden) values (" + str(id_polling) + ",'"
            + str(waktu_mulai) + "','" + str(waktu_selesai) + "'," + str(0) + ");" )
        cursor.execute(query)

       	#insert new polling biasa
        query = ("insert into polling_biasa (id_polling, url, deskripsi) values (" + str(id_polling) + ",'" + str(url) + "','"
            + str(deskripsi) +"');")
       	cursor.execute(query)

        messages.success(request, "Polling berhasil dibuat!")
        return HttpResponseRedirect(reverse('lihat-polling:index'))
    else :
    	return HttpResponse("null")
