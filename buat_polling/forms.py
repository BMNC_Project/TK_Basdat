from django import forms
from .models import *

class FormPollingBerita(forms.Form):
    url_berita = forms.URLField(label='URL berita', max_length=100, required=True)
    tanggal_mulai = forms.CharField(label='Tanggal mulai', max_length=100, required=True)
    tanggal_selesai = forms.CharField(label='Tanggal selesai', max_length=100, required=True)
    pertanyaan = forms.CharField(label='Pertanyaan', max_length=100, required=True)
    pilihan = forms.CharField(label='Pilihan', max_length=100, required=True)


class FormPollingBiasa(forms.Form):
    url = forms.URLField(label='URL berita', max_length=100, required=True)
    deskripsi = forms.CharField(label='Deskripsi', max_length=100, required=True)
    waktu_mulai = forms.CharField(label='Waktu mulai', max_length=100, required=True)
    waktu_selesai = forms.CharField(label='Waktu selesai', max_length=100, required=True)
    pertanyaan = forms.CharField(label='Pertanyaan', max_length=100, required=True)
    pilihan = forms.CharField(label='Pilihan', max_length=100, required=True)
